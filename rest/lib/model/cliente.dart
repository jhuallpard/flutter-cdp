class Client {
  int id;
  String name;
  String phone;

  Client({this.id, this.name, this.phone});

  Map<String, dynamic> toMap() => {
        "id": id,
        "name": name,
        "phone": phone,
      };

  factory Client.fromMap(Map<String, dynamic> json) => new Client(
        id: json["id"],
        name: json["name"],
        phone: json["phone"],
      );
}
